import imp
import inspect
from Log import Log

logger = Log(__name__)

modules = ["InfluxDbCommunicator", "MQTTHomeAssistantCommunicator"] #"InfluxDbCommunicator", "MQTTHomeAssistantCommunicator"]

class CommLoader():
    def __init__(self):
        self._objs = []
        for m in modules:
            fp, pathname, description = imp.find_module(m, ["./Communicators"])
            logger.info(pathname)
            module = imp.load_module(m, fp, pathname, description)
            for _, obj in inspect.getmembers(module):
                if inspect.isclass(obj) and inspect.getmodule(obj) is module:
                    self._objs.append(obj())
                    break
            if fp:
                fp.close()

    def update(self, data):
        for obj in self._objs:
            obj.update(data)