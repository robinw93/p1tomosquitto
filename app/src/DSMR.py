from Log import Log

import re
import crcmod.predefined
import Utils as u

logger = Log(__name__)

pattern = r'\r\n(?=!)'
crc16 = crcmod.predefined.mkPredefinedCrcFun('crc16')

class DSMR:
  def __init__(self, payload):
    self._valid = False
    self._workablePayload = self.parse(payload)
    self._parsedPayload = Entry(self._workablePayload)

  @property
  def valid(self):
    return self._valid

  @property
  def payload(self):
    return self._parsedPayload

  def parse(self, telegram):
    # We have a complete telegram, now we can process it.
    # Look for the checksum in the telegram
    hashpattern = r'!([A-Z0-9]*)'
    matches = re.finditer(hashpattern, telegram)
    for m in matches:
      try:
        given_checksum = int('0x' + m.group(1), 16)
      except ValueError:
        logger.warning("Checksum could not be casted to int. contained probably characters.")
        given_checksum = 0
      # The exclamation mark is also part of the text to be CRC16'd
      calculated_checksum = crc16(telegram[:m.start() + 1].encode('utf-8'))
      if given_checksum == calculated_checksum:
        self._valid = True

    telegram_values = {}
    if self._valid:
        # Store the vaules in a dictionary
        # Split the telegram into lines and iterate over them
        for telegram_line in telegram.split('\r\n'):
            if len(telegram_line) == 0:
                continue
            topic = re.match('([^(]*)', telegram_line)
            topic = topic.group(1) if topic is not None else ""

            isHash = len(re.findall(hashpattern, str(topic))) > 0
            if isHash:
              continue

            values = []
            for match in re.findall('\((.*?)\)', telegram_line):
                values.append(match)

            # print(f"{topic}: {values}")
            telegram_values[topic] = values
    return telegram_values

codes_to_topic = {
  '1-0:1.8.1': "total_energy_consumed_t1",
  '1-0:1.8.2': "total_energy_consumed_t2",
  '1-0:2.8.1': "total_energy_produced_t1",
  '1-0:2.8.2': "total_energy_produced_t2",
  '0-0:96.14.0': "energy_tariff_indication",
  '1-0:1.7.0': "actual_consumed_power",
  '1-0:2.7.0': "actual_produced_power",
  '0-0:17.0.0': "actual_threshold",
  '0-0:96.3.10': "switch_position",
  '0-0:96.7.21': "total_power_failures",
  '0-0:96.7.9': "total_long_power_failures",
  '1-0:32.32.0': "voltage_sags_l1",
  '1-0:52.32.0': "voltage_sags_l2",
  '1-0:72:32.0': "voltage_sags_l3",
  '1-0:32.36.0': "voltage_swells_l1",
  '1-0:52.36.0': "voltage_swells_l2",
  '1-0:72.36.0': "voltage_swells_l3",
  '1-0:31.7.0': "current_l1",
  '1-0:51.7.0': "current_l2",
  '1-0:71.7.0': "current_l3",
  '1-0:21.7.0': "insta_power_consumed_l1",
  '1-0:41.7.0': "insta_power_consumed_l2",
  '1-0:61.7.0': "insta_power_consumed_l3",
  '1-0:22.7.0': "insta_power_produced_l1",
  '1-0:42.7.0': "insta_power_produced_l2",
  '1-0:62.7.0': "insta_power_produced_l3",
  '0-1:24.2.1': "gas",
  '0-1:96.1.0': "gas_meter_id",
  '0-1:24.1.0': "gas_number",
  '1-3:0.2.8': "version_info",
  '0-0:1.0.0': "timestamp",
  '0-0:96.1.1': "identifier",
  '1-0:99.97.0': "long_power_failure_event_log",
  '0-0:96.13.0': "text_message",
  '1-0:32.7.0': "instantaneous_voltage_l1",
  '1-0:52.7.0': "instantaneous_voltage_l2",
  '1-0:72.7.0': "instantaneous_voltage_l3"
}

ignore_codes = [
  "/CTA5ZIV-METER"
]

class Entry:
  def __init__(self, telegram):
    self._data = self.parse(telegram)

  def __str__(self):
    data = []
    for k,v in self._data.items():
      data.append(f"{k} - {v}")
    return '\n'.join(data)

  @property
  def data(self):
    return self._data

  def parse(self, telegram):
    parsedPayload = {}
    for key, value in telegram.items():
      topic = codes_to_topic.get(key, None)

      if (topic == None):
        if key not in ignore_codes:
          logger.debug(f"Could not find {key}")
      else:
        parsedPayload[topic] = value

    timeObj = parsedPayload.get("timestamp", [None])[0]
    for key, value in parsedPayload.items():
      parsedPayload[key] = Value(timeObj, value)

    return parsedPayload

multiplier = {
    'm3'  : 1,
    'W'   : 1,
    'kW'  : 1000,
    'kWh' : 1000,
    'MWh' : 1000000,
    'V'   : 1,
    's'   : 1,
    'A'   : 1,
    'kA'  : 1000,
    'MA'  : 1000000,
    None  : 1,
  }

unit = {
    'm3'  : 'm3',
    'W'   : 'W',
    'kW'  : 'W',
    'Wh'  : "Wh",
    'kWh' : 'Wh',
    'MWh' : 'Wh',
    'V'   : 'V',
    's'   : 's',
    'A'   : 'A',
    'kA'  : 'A',
    'MA'  : 'A',
    None  : "NaN",
  }

class Value:
  def __init__(self, time, data):
    self._value = self.parseValue(data)
    self._unit = self.parseUnit(data)
    self._time = self.parseTime(time, data)

  def __str__(self):
    valueStr = self._value
    if (self._value == float('inf')):
      valueStr = None
    return f"{self._time} - {valueStr} - {self._unit} - {self.value}"

  @property
  def value(self):
    return self._value * multiplier[self._unit]

  @property
  def unit(self):
    return unit[self._unit]

  @property
  def time(self):
    return self._time

  def parseValue(self, data):
    val = None
    for d in data:
      if "*" in d:
        idx = d.find("*")
        if idx != -1:
          val = d[:idx]
          break

    if not val and len(data) == 1 and "W" not in data[0]:
      val = data[0]

    if not val:
      val = 'inf'

    try:
      val = float(val)
    except:
      val = 'inf'

    return val

  def parseUnit(self, data):
    unit = None
    for d in data:
      if "*" in d:
        idx = d.find("*")
        if idx != -1:
          unit = d[idx+1:]
          break

    return unit

  def parseTime(self, time, data):
    # gas - ['220221225500W', '00822.510*m3']
    # W is DST, S is no DST
    datetime = None
    for d in data:
      if ("W" in d or "S" in d) and not "*" in d:
        datetime = u.influxTimeToDateTime(d)
        break

    if datetime == None and time != None:
      datetime = u.influxTimeToDateTime(time)

    return datetime


