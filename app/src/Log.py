# Use it in your other modules as:
  # from Log import Log
  # logger = Log(__name__)

import logging
from logging import handlers
import os

LOG_LEVEL = logging.INFO

logger = logging.getLogger()
logger.setLevel(LOG_LEVEL)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

ch = logging.StreamHandler()
ch.setLevel(LOG_LEVEL)
ch.setFormatter(formatter)
logger.addHandler(ch)

filename = "./log/log.txt"
should_roll_over = os.path.isfile(filename)
rfh = handlers.RotatingFileHandler(filename, maxBytes=5 * 1000 * 1000, backupCount=5)
if should_roll_over:
  rfh.doRollover()
rfh.setLevel(LOG_LEVEL)
rfh.setFormatter(formatter)
logger.addHandler(rfh)


class Log():
  def __init__(self, name):
    self.logger = logging.getLogger(name)

    self.info(f"Instantiated logger for {name}")

  def critical(self, msg):
    self.logger.critical(msg)

  def fatal(self, msg):
    self.logger.fatal(msg)

  def error(self, msg):
    self.logger.error(msg)

  def warning(self, msg):
    self.logger.warning(msg)

  def warn(self, msg):
    self.logger.warn(msg)

  def info(self, msg):
    self.logger.info(msg)

  def debug(self, msg):
    self.logger.debug(msg)