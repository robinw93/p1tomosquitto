from influxdb import InfluxDBClient
from influxdb.exceptions import InfluxDBServerError, InfluxDBClientError
from requests.exceptions import ConnectionError

import Utils as u
from Log import Log

logger = Log(__name__)

class InfluxDbConnection:

  def __init__(self, database, username, password, host, port, stayConnected = True):
    self.database = database
    self.username = username
    self.pw = password
    self.host = host
    self.port = port
    self.stayConnected = stayConnected

    self.data = []

  def connect(self):
    try:
      self._client = InfluxDBClient(host=self.host, port=self.port, username=self.username, password=self.pw, database=self.database)
      # self._client = client.write_api(write_options=SYNCHRONOUS)
    except Exception as e:
      self._e = e
      logger.warn("Can not connect to InfluxDb Server: {e}")
      raise e

  def query(self, query):
    return self._client.query(query)

  def add(self, measurement, time, tags, fields):
    e = {}
    e["measurement"] = measurement
    e["time"] = u.toInfluxTime(time)
    e["tags"] = tags
    e["fields"] = fields

    logger.debug(e)

    self.data.append(e)

  def commit(self):
    for d in self.data:
      logger.debug(d)

    self.connect()
    if not self._connected():
      logger.warn(f"Could not connect to InfluxDb Server: {self._e}")
      return
    try:
      self._client.write_points(self.data, time_precision='s')
    except ConnectionError as e:
      logger.error(e)
      return
    except InfluxDBServerError as e:
      logger.error(e)
      if hasattr(e, "code") and e.code == 404:
        return
    except InfluxDBClientError as e:
      logger.error(e)
      return

    except Exception as e:
      raise e

    if not self.stayConnected:
      self._client.close()

    self.data = []

  def _connected(self):
    try:
      return self._client.ping() != None
    except Exception as e:
      self._e = e
      return False