from Log import Log
from datetime import datetime, timedelta
import threading as th

logger = Log(__name__)

class DailyTask:
  def __init__(self, hour, minute, cb) -> None:
    self.timer = None
    self.hour = hour
    self.minute = minute
    self.cb = cb

  def restartPeriodicTask(self):
    self.startPeriodicTask()

  def startPeriodicTask(self):
    updateAt, diff = self._timeToNextUpdate()
    logger.info(f"Update Task in: {updateAt}, that takes {diff} seconds!")
    # self._timer = th.Timer(10, self.cb)
    self._timer = th.Timer(diff, self.cb)
    self._timer.start()

  def _timeToNextUpdate(self):
    now = datetime.now()
    updateAt = (now + timedelta(days=1)).replace(hour=self.hour, minute = self.minute, second = 0)
    diff = updateAt - now
    return updateAt, diff.total_seconds()