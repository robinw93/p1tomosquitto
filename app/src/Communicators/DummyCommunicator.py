from Log import Log


logger = Log(__name__)

filter = ["version_info", "timestamp", "identifier", "text_message", "gas_number", "gas_meter_id"]

class DummyCommunicator():
    def __init__(self):
        logger.info("initialized dummy communicator")

    def update(self, message):
        for subject, entry in message.data.items():
            if subject not in filter:
                logger.info(f"{subject} {entry}")