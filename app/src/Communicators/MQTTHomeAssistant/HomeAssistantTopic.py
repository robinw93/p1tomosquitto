
from multiprocessing.sharedctypes import Value
from Log import Log
import json

logger = Log(__name__)

BASE_TOPIC = "homeassistant"

class MQTTHomeAssistantTopic():

  def __init__(self, name: str, unitOfMeasurement: str):
    self._base = f"{BASE_TOPIC}/sensor/{name}"
    self._name = name
    self._config = f"{self._base}/config"
    self._state = f"state"
    self._payload = f"config"
    self._unitOfMeasurement = unitOfMeasurement
    self._multiplier = 1

  def announcePayload(self):
    deviceClass = None
    if self._unitOfMeasurement == "W":
      deviceClass = "power"
      self._multiplier = 1/1E3
      self._unitOfMeasurement = "kW"
    elif self._unitOfMeasurement == "kW":
      deviceClass = "power"
      self._multiplier = 1
      self._unitOfMeasurement = "kW"
    elif self._unitOfMeasurement == "MWh":
      deviceClass = "energy"
      self._unitOfMeasurement = "kWh"
      self._multiplier = 1E3
    elif self._unitOfMeasurement == "kWh":
      deviceClass = "energy"
      self._unitOfMeasurement = "kWh"
      self._multiplier = 1
    elif self._unitOfMeasurement == "Wh":
      deviceClass = "energy"
      self._unitOfMeasurement = "kWh"
      self._multiplier = 1/1E3
    elif self._unitOfMeasurement == "m3": 
      self._unitOfMeasurement = "m³"
      deviceClass = "gas"
    elif self._unitOfMeasurement == "s": 
      deviceClass = "duration"
    elif self._unitOfMeasurement == "A": 
      deviceClass = "current"
    elif self._unitOfMeasurement == "V": 
      deviceClass = "voltage"

    stateClass = "measurement"
    if ("total" in self._name) or (self._name == "gas"):
      stateClass = "total_increasing"

    payload = {}
    if deviceClass:
      payload["device_class"] = deviceClass
    if deviceClass == "energy":
      self._name += "_energy"
    payload["name"] = self._name
    payload["~"] = self._base
    payload["state_topic"] = f"~/state"
    payload["unit_of_measurement"] = self._unitOfMeasurement
    payload["state_class"] = stateClass

    # payload = {}

    return self._config, json.dumps(payload)

  def getUpdateTopicAndValue(self, data):
    return f"{self._base}/{self._state}", data.value*self._multiplier