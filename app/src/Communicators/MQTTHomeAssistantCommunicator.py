
import paho.mqtt.client as mqtt
from Log import Log
from Communicators.MQTTHomeAssistant import HomeAssistantTopic as HAT

from DSMR import DSMR

logger = Log(__name__)

#mqtt settings
clientName = "p1dsmr"
host = "192.168.2.200"
port=1883

filter = ["version_info", "timestamp", "identifier", "text_message", "gas_number", "gas_meter_id"]

class MQTTHomeAssistantCommunicator():
  def __init__(self):
    self._connected = False
    self._client = mqtt.Client(clientName)
    self._client.on_connect = self.on_connect
    self._client.on_disconnect = self.on_disconnect
    self._MQTTObjects : DSMR = {}
    self.mosquittoConnect()
    logger.info("Initialized MQTTHomeAssistantCommunicator")

  def on_connect(self, client, userdata, flags, rc):
    self._connected = True
    logger.info("MQTT client connected")

  def on_disconnect(self, client, userdata, rc):
    self._connected = False
    logger.info("MQTT client disconnected")

  def mosquittoConnect(self):
    try:
      self._client.connect(host, port=port, keepalive=60)
      self._client.loop_start()
    except OSError as e:
      logger.warn(f"Could not start MQTT: {e}")

  def update(self, message):
    if not self._connected:
      self.mosquittoConnect()
    else:
      entry: DSMR
      subject: str
      for subject, entry in message.data.items():
        if subject not in filter:
          obj : HAT.MQTTHomeAssistantTopic = self._MQTTObjects.get(subject, None)
          if obj == None:
            self._MQTTObjects[subject] = HAT.MQTTHomeAssistantTopic(subject, entry.unit)
            obj = self._MQTTObjects[subject]
            topic, payload = obj.announcePayload()
            self._client.publish(topic, payload, retain=True)
            logger.info(f"Announce: {topic} - {payload}")

          topic, value = obj.getUpdateTopicAndValue(entry)

          logger.debug(f"{topic} {value}")
          self._client.publish(topic, str(value))








