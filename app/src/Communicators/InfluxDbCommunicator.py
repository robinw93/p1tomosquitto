from Communicators.InfluxDb import InfluxDbConnection as db
from Communicators.InfluxDb import DailyTask
import Utils as u


from datetime import datetime, timedelta

from Log import Log
import logging
import os

urrlibLogger = logging.getLogger("urllib3.connectionpool")
urrlibLogger.setLevel(logging.INFO)

logger = Log(__name__)

# dbName = "power"
host = os.getenv("INFLUX_HOST", None)
port = 8086
user = os.getenv("INFLUX_USER", None)
pw = os.getenv("INFLUX_PASS", None)

CONTINUOUS_DATABASE = "second"
DAILY_DATABASE = "daily"

blacklist = ["version_info", "timestamp", "identifier", "text_message", "gas_number", "gas_meter_id"]

class InfluxDbCommunicator():
  def __init__(self):
    self._client = None
    self._e = None

    self.continuousConnection = db.InfluxDbConnection(CONTINUOUS_DATABASE, user, pw, host, port, stayConnected=True)
    self.continuousConnection.connect()

    self.dailyConnection = db.InfluxDbConnection(DAILY_DATABASE, user, pw, host, port, stayConnected=False)
    self.dailyConnection.connect()

    self.dailyTask = DailyTask.DailyTask(1,0, self.timerCb)
    self.dailyTask.startPeriodicTask()

  def update(self, message):
    for subject, entry in message.data.items():
      if subject not in blacklist:
        measurement = subject
        time = entry.time
        tags = {"unit": entry.unit}
        fields = {"value": entry.value}
        self.continuousConnection.add(measurement, time, tags, fields)

    self.continuousConnection.commit()

  def _getData(self, timestamp, measurement):
    logger.info(f"Searching for: {timestamp}")

    query = f'select * from "{measurement}" where time >= {timestamp.strftime("%s")}000ms - 5s and time <= {timestamp.strftime("%s")}000ms + 5s'
    records = self.continuousConnection.query(query)

    if len(list(records.get_points())) == 0:
      logger.debug(f"Query unsuccesfull: {query}")
      return None, None, None

    selectedValue = 0
    minimum = 100
    for r in records.get_points():
      logger.info(r)
      time = r["time"]
      date = datetime.strptime(time[:len(time)-1], "%Y-%m-%dT%H:%M:%S")
      diff = (timestamp - date).total_seconds()

      if abs(diff) < minimum:
        minimum = abs(diff)
        selectedValue = r
      # logger.info(f"{now} - {date} = {now-date}")

    return selectedValue["value"], selectedValue["unit"], diff

  def timerCb(self):
    now = datetime.now()
    logger.info(f"Updating daily database at: {now}")

    topicsOfInterest = ["gas", "total_energy_consumed_t1", "total_energy_consumed_t2", "total_energy_produced_t1", "total_energy_produced_t2"]
    for topic in topicsOfInterest:
      difference, unit = self.delta(now, topic)

      if difference != None:
        measurement = topic
        time1 = ((datetime.now() - timedelta(days=1)).replace(hour=00, minute=5, second=0))
        time2 = ((datetime.now() - timedelta(days=1)).replace(hour=12, minute=0, second=0))
        time3 = ((datetime.now() - timedelta(days=1)).replace(hour=23, minute=55, second=0))
        logger.info(f"time: {time1} - {time2} - {time3}")
        tags = {"unit": unit}
        fields = {"value": difference/1000}

        self.dailyConnection.add(measurement, time1, tags, fields)
        self.dailyConnection.add(measurement, time2, tags, fields)
        self.dailyConnection.add(measurement, time3, tags, fields)

        logger.info(f"This day \"{topic}\" : {difference} {unit}")
      else:
        logger.error(f"Found no delta for {topic}")

    self.dailyConnection.commit()


    self.dailyTask.restartPeriodicTask()

  def delta(self, now, topic):
    # Gather interesting topic data.
    endDate = (now - timedelta(days=1)).replace(hour=23, minute=59, second=59)
    startDate = (now - timedelta(days=2)).replace(hour=0, minute=0, second=0)

    valueStart, unit, error = self._getData(startDate, topic)
    valueEnd, _, error = self._getData(endDate, topic)

    if valueStart == None or valueEnd == None:
      logger.error(f"Could not determine start or end value. Start: {valueStart}, End: {valueEnd}")
      return None

    return valueEnd - valueStart, unit
