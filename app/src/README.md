This repo extracts the p1 data from the serial port of the " slimme meter" and places these messages onto MQTT topics.
Coded in python and build for the " Enexis " meter, but it probably also works for other smart meters as well.

Author Robin
