import serial
import re
from Log import Log

logger = Log(__name__)

PORT = "/dev/ttyUSB0"
BAUDRATE = 115200

class Serial:
  def __init__(self):
    self.ser = serial.Serial(PORT, BAUDRATE)
    self.ser.bytesize=serial.SEVENBITS
    self.ser.parity=serial.PARITY_EVEN
    self.ser.stopbits=serial.STOPBITS_ONE
    self.ser.xonxoff=0
    self.ser.rtscts=0
    self.ser.timeout=20

  def telegram(self):
    if not self.ser.isOpen():
      logger.info(f"Opening Serial port: {PORT}")
      self.ser.open()

    checksum_found = False
    telegram = ""
    while not checksum_found:
        telegram_line = self.ser.readline()
        telegram_line = telegram_line.decode('utf-8')

        if re.match('(?=!)', telegram_line):
            telegram += (telegram_line)
            checksum_found = True
        else:
            telegram += (telegram_line)
    logger.debug(telegram)
    return telegram