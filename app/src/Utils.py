import datetime
from datetime import datetime, timedelta

def toInfluxTime(time):
  return time.strftime("%Y-%m-%dT%H:%M:%SZ")

def influxTimeToDateTime(strDate):
    date = datetime.strptime(strDate[:len(strDate)-1], "%y%m%d%H%M%S")
    # Convert to UTC:
    # With DST (S), then UTC + 2
    # With no DST (W), then UTC + 1
    if "S" in strDate:
      date = date - timedelta(hours=2)
    elif "W" in strDate:
      date = date - timedelta(hours=1)

    return date