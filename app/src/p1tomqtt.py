
#!/usr/bin/env python3
# Python script to retrieve and parse a DSMR telegram from a P1 port

import traceback

# import paho.mqtt.client as mqtt

import time
from Log import Log
from DSMR import DSMR
from Serial import Serial
from CommLoader import CommLoader
# Debugging settingsd

logger = Log("P1-DSMR")
serial = Serial()

#global vars
timer_expire = 300 # at least and updaterate of once every "timer_expire" seconds.
time_prev = time.time(); #higher than the expiring timer.

def getP1Message():
    telegram = serial.telegram()
    data = DSMR(telegram)
    return data.valid, data.payload

def main():
    communicators = CommLoader()

    while True:
        good_checksum, message = getP1Message() # Get the P1 message with checked for checksum.

        if (good_checksum):
            communicators.update(message)
        else:
            logger.debug("No correct checksum, skipping.")


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.error(f"main crashed. Error : {e}")
        traceback.print_exception(type(e), e, e.__traceback__)

